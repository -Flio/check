﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageFilther : MonoBehaviour
{
    private Image _image;
    private ImageTransformation _filther;
    public Slider slider;
    // Start is called before the first frame update
    void Start()
    {
        _image =  gameobject.GetComponent<Image>();
        _filther = gameobject.GetComponent<ImageTransformation>();
       
    }


    public void FBlur()
    {
        _image.sprite = _filther.ToBlur(_image.sprite, slider.value);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
