﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ImageTransformation : MonoBehaviour
{
    [SerializeField] public ComputeShader _computeShader;
    //[SerializeField] public SpriteRenderer spriteRenderer = null;
    [SerializeField] public Sprite sprite;
    [SerializeField] public Filters filter;
    [SerializeField] public int windowSize;
    [SerializeField] public bool brightness;
    [SerializeField] public int medianK;
    [SerializeField] public float sigma;


    RenderTexture rwDebug;
    // Start is called before the first frame update
    void Start()
    {
        
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Sprite ToBlur(Sprite origin,int windowSize)
    {
        var kernel = _computeShader.FindKernel("ToBlur");
        return ApplyFilther(_computeShader, kernel, origin, windowSize);
    }

    public Sprite SobolFilther(Sprite origin, bool brightness)
    {
        var kernel = _computeShader.FindKernel("Sobol");
        _computeShader.SetBool("brightness", brightness);
        return ApplyFilther(_computeShader, kernel, origin);
    }

    public Sprite PrevittaFilther(Sprite origin, bool brightness)
    {
        var kernel = _computeShader.FindKernel("Previtta");
        _computeShader.SetBool("brightness", brightness);
        return ApplyFilther(_computeShader, kernel, origin);
    }

    public Sprite RobertsaFilther(Sprite origin, bool brightness)
    {
        var kernel = _computeShader.FindKernel("Robertsa");
        _computeShader.SetBool("brightness", brightness);
        return ApplyFilther(_computeShader, kernel, origin);    
    }

    public Sprite MedianFilther(Sprite origin, int windowSize, int medianK)
    {
        var kernel = _computeShader.FindKernel("Median");
        _computeShader.SetInt("MedianK", medianK);
        return ApplyFilther(_computeShader, kernel, origin, windowSize);
    }

    public Sprite Median2Filther(Sprite origin, int windowSize, int medianK)
    {
        var kernel = _computeShader.FindKernel("Median2");
        _computeShader.SetInt("MedianK", medianK);
        return ApplyFilther(_computeShader, kernel, origin, windowSize);
    }

    public Sprite Gauss(Sprite origin, int windowSize, float sigma)
    {
        var kernel = _computeShader.FindKernel("Gauss");
        _computeShader.SetFloat("Sigma", sigma);
        return ApplyFilther(_computeShader, kernel, origin, windowSize);
    }

    Texture2D toTexture2D(RenderTexture rTex, TextureFormat texF = TextureFormat.RGB24)
    {
        Texture2D tex = new Texture2D(rTex.width, rTex.height, texF, false);
        RenderTexture.active = rTex;
        tex.ReadPixels(new Rect(0, 0, rTex.width, rTex.height), 0, 0);
        tex.Apply();
        return tex;
    }

    private Sprite ApplyFilther( ComputeShader computeShader, int kernel, Sprite origin, int windowSize = 0)
    {
        Texture2D texture = origin.texture;
        RenderTexture result = new RenderTexture(texture.width, texture.height, 1);
        result.enableRandomWrite = true;
        result.Create();

        computeShader.SetTexture(kernel, "Result", result);
        computeShader.SetTexture(kernel, "Origin", texture);
        computeShader.SetVector("PixelSize", new Vector2(texture.width, texture.height));
        computeShader.SetInt("WindowSize", windowSize);
        computeShader.Dispatch(kernel, texture.width / 8, texture.height / 8, 1);

        var piv = new Vector2(0.5f, 0.5f);
        return Sprite.Create(toTexture2D(result, TextureFormat.RGBA32), origin.rect, piv);
    }


    


    //private void TestBlur(Texture2D Result, Texture2D Orign, Vector2Int PixelSize, int WindowSize, Vector2Int id)
    //{
    //    if (WindowSize % 2 == 1)
    //        WindowSize = WindowSize - 1;

    //    int halfWindowSize = WindowSize / 2;

    //    int firstX = id.x - halfWindowSize;
    //    if (firstX < 0)
    //        firstX = 0;

    //    int lastX = id.x + halfWindowSize;
    //    if (lastX > PixelSize.x)
    //        lastX = PixelSize.x;

    //    int firstY = id.y - halfWindowSize;
    //    if (firstY < 0)
    //        firstY = 0;

    //    int lastY = id.y + halfWindowSize;
    //    if (lastY > PixelSize.y)
    //        lastY = PixelSize.y;


    //    Color color = new Color(0, 0, 0, 0);
    //    int count = 0;

    //    for (int x = firstX; x <= lastX; x++)
    //    {
    //        for (int y = firstY; y <= lastY; y++)
    //        {
    //            var pixel = Orign.GetPixel(x, y);
    //            //color.x = 
    //            color = color + pixel;
    //            count++;
    //        }
    //    }
    //    color = color / count;

    //    //Texture2D tex = new Texture2D(rTex.width, rTex.height, texF, false);

    //    Result.SetPixel(id.x, id.y, color);
    //}

    public enum Filters
    {
        BoxBlur,
        Gauss,
        Sobol,
        Previtta,
        Robertsa,
        Median,
        Median2
    }
}
