﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public Animator OpenedMenu;
    

    

    public void OpenMenu()
    {
        
        OpenedMenu.SetBool("isHidden", false);
        
    }
    public void CloseMenu()
    {

        OpenedMenu.SetBool("isHidden", true);

    }

}
