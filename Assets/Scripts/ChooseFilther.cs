﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChooseFilther : MonoBehaviour
{
    [SerializeField] private Material _simple;
    [SerializeField] private Material _gray;
    

    private SpriteRenderer _sr;
    private void Start()
    {
        _sr = gameObject.GetComponent<SpriteRenderer>();
    }

    public void ToGray()
    {
        _sr.material = _gray;
    }
    
    public void ToSimple()
    {
        _sr.material = _simple;
    }

    public void ChangeGray(Slider slider)
    {
        _gray.SetFloat("_GrayValue",1 - slider.value);
    }
}
