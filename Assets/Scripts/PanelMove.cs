﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class PanelMove : MonoBehaviour
{
    [SerializeField] private float _distance;

    [SerializeField] private float _duration;

    [SerializeField] private Sprite Arrow1;
    [SerializeField] private Sprite Arrow2;

    [SerializeField] private Image _buttonImage;
    // Start is called before the first frame update

    private float _xPos;
    private Action _motion;
    private void Start()
    {
        _buttonImage.sprite = Arrow2;
        _xPos = this.transform.position.x;
        _motion = Open;
    }

    public void PanelMotion()
    {
        _motion();
    }

    public void Open()
    {
        Sequence seq = DOTween.Sequence();
        seq.Append(this.transform.DOMoveX(transform.position.x + _distance, _duration));
        seq.AppendCallback((() =>
        {
            _motion = Close;
            _buttonImage.sprite = Arrow1;

        }));
        seq.Play();
    }

    public void Close()
    {
        Sequence seq = DOTween.Sequence();
        seq.Append(this.transform.DOMoveX(_xPos, _duration));
        seq.AppendCallback((() =>
        {
            _motion = Open;
            _buttonImage.sprite = Arrow2;

        }));
        seq.Play();
    }
}
