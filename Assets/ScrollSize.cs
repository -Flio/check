﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ScrollSize : MonoBehaviour
{
    [SerializeField]
    private GameObject _content;

    [SerializeField] private float min;

    [SerializeField] private float max;
    [SerializeField] private float x;

    private Transform[] _objs;
    // Start is called before the first frame update
    void Start()
    {
        _objs = _content.GetComponentsInChildren<Transform>().Where( t => t != _content.transform).ToArray();
    }

    // Update is called once per frame
    void Update()
    {
        foreach (var obj in _objs)
        {
            var scale = Mathf.Lerp(max, min, Mathf.Abs(x - obj.position.x));
            obj.transform.localScale = new Vector3(scale,scale,1);
        }
    }
}
