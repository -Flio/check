﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BigImage : MonoBehaviour
{
    public void SetImage(Sprite sprite)
    {
        var im = GetComponent<Image>();
        im.sprite = sprite;
        im.preserveAspect = true;
    }
}
