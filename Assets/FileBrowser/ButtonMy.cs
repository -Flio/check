﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonMy : MonoBehaviour
{
    private Sprite _sprite;
    private Button _button;
    private Action<Sprite> _action;

    public void Init(Sprite sprite, Action<Sprite> action)
    {
        _sprite = sprite;
        _action = action;
        Button bt = GetComponent<Button>();
        if (bt == null)
        {
            bt = this.gameObject.AddComponent<Button>();
        }

        bt.onClick.AddListener(DoAction);
    }

    private void DoAction()
    {
        _action.Invoke(_sprite);
    }
}
