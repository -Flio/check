﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Collections;
using System.Linq;
using UnityEngine;
using SimpleFileBrowser;
using UnityEngine.Events;
using UnityEngine.UI;

[Serializable]
public class ToBigImageEventClass : UnityEvent<Sprite>{}
public class FilesToSprites : MonoBehaviour
{
    [SerializeField] private Transform _parent;

    [SerializeField] private GameObject Pref;

    public ToBigImageEventClass ToBigImageEventClass;
    // Start is called before the first frame update

    public void OpenFile()
    {
        //todo: image filther
        FileBrowser.SetFilters(true, new FileBrowser.Filter("Images", ".jpg", ".png"));
        FileBrowser.SetDefaultFilter( ".jpg" );
        FileBrowser.ShowLoadDialog(SetFiles, (() => { }), true);
    }
    
    private void SetFiles(string filePath)
    {
        foreach (Transform trans in _parent)
        {
            Destroy(trans.gameObject);
        }

        if (filePath.EndsWith(".jpg") || filePath.EndsWith(".png"))
        {
            ToBigImageEventClass.Invoke(IMG2Sprite.instance.LoadNewSprite(filePath));
            return;
        }
        
        var imagesPath = ProcessDirectory(filePath);

        foreach (var path in imagesPath)
        {
            GameObject go;
            Image sr;
            ButtonMy bt;
            if (Pref == null)
            {
                go = new GameObject();
                go.transform.SetParent(_parent);
                go.AddComponent<RectTransform>();
                sr = go.AddComponent<Image>();
                bt = go.GetComponent<ButtonMy>();
            }
            else
            {
                go = Instantiate(Pref, _parent);
                sr = go.GetComponent<Image>();
                if (sr == null)
                {
                    sr = go.AddComponent<Image>();
                }
                bt = go.GetComponent<ButtonMy>();
                if (bt == null)
                {
                    bt = go.AddComponent<ButtonMy>();
                }
            }
            
            sr.sprite = IMG2Sprite.instance.LoadNewSprite(path);
            //sr.preserveAspect = true;
            bt.Init(sr.sprite,(s) => {ToBigImageEventClass.Invoke(s);});
        }
        
    }
    
    public static List<string> ProcessDirectory(string targetDirectory)
    {
        List<string> imagesPath = new List<string>();
        
        // Process the list of files found in the directory.
        string [] fileEntries = Directory.GetFiles(targetDirectory);
        foreach(string fileName in fileEntries)
            imagesPath.Add(fileName);

        return imagesPath.Where( s => s.EndsWith(".jpg") || s.EndsWith(".png")).ToList();

        // Recurse into subdirectories of this directory.
        // string [] subdirectoryEntries = Directory.GetDirectories(targetDirectory);
        // foreach(string subdirectory in subdirectoryEntries)
        //     ProcessDirectory(subdirectory);
    }
}
